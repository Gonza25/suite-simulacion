using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisparoManager : MonoBehaviour
{
    public GameObject Cannon;
    public TMPro.TMP_InputField InputField;
    float angulo;
    public void IniciarDisparo()
    {
        angulo = float.Parse(InputField.text);
        Cannon.GetComponent<CañonScript>().SetRotation(angulo);
    }
}
