using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BolaScript : MonoBehaviour
{
    public GameObject Explosion;
    private void OnCollisionEnter(Collision collision)
    {
        Explosion = Instantiate(Explosion, transform.position, transform.rotation);
        Destroy(this.gameObject);
    }
}
