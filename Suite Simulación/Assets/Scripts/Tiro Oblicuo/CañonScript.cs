using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CañonScript : MonoBehaviour
{
    public GameObject Proyectil, ShootingPoint;
    public float fuerza;
    public TMPro.TMP_InputField InputField;
    void Start()
    {
        transform.rotation = Quaternion.Euler(0f, 0f, 0f);
    }

    public void SetRotation(float angulo)
    {
        if (angulo >= 0f && angulo <= 90f)
        {
            transform.rotation = Quaternion.Euler(0f, 0f, angulo + 270f);
        }
        
        if (angulo < 0f)
        {
            transform.rotation = Quaternion.Euler(0f, 0f, 270f);
        }

        if (angulo > 90f)
        {
            transform.rotation = Quaternion.Euler(0f, 0f, 0f);
        }
    }

    public void Shoot()
    {
        fuerza = float.Parse(InputField.text);

        GameObject ProyectilDisparado;

        ProyectilDisparado = Instantiate(Proyectil, ShootingPoint.transform.position, ShootingPoint.transform.rotation);

        Rigidbody body = ProyectilDisparado.GetComponent<Rigidbody>();

        body.AddForce(this.transform.up * fuerza, ForceMode.Impulse);
    }
}
