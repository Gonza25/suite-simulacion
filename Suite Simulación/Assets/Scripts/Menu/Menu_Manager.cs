using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu_Manager : MonoBehaviour
{
    short decoy;
    [Header("--- Pantallas de Men� ---\n")]

    [SerializeField] private GameObject titleScreen;
    [SerializeField] private GameObject menuPrincipal;
    [SerializeField] private GameObject opciones;
    [SerializeField] private GameObject creditos;
    [SerializeField] private GameObject proyectSelect;




    void Start()
    {
        TitleScreenActive();
    }

    // Update is called once per frame
    void Update()
    {
        saltarTitleScreen();
    }


    #region METODOS DE BOTONES
    /// <summary>
    /// M�todo encargado de ocultar al principio todas las pantallas menos la de inicio.
    /// </summary>
    private void TitleScreenActive()
    {
        titleScreen.SetActive(true);
        menuPrincipal.SetActive(false);
        opciones.SetActive(false);
        creditos.SetActive(false);
        proyectSelect.SetActive(false);
    }
    /// <summary>
    /// M�todo encargado de mostrar el Menu Principal.
    /// </summary>
    public void MenuPrincipalActive()
    {
        titleScreen.SetActive(false);
        menuPrincipal.SetActive(true);
        opciones.SetActive(false);
        creditos.SetActive(false);
        proyectSelect.SetActive(false);
    }
    /// <summary>
    /// M�todo encargado de mostrar el Menu de opciones.
    /// </summary>
    public void OpcionesActive()
    {
        titleScreen.SetActive(false);
        menuPrincipal.SetActive(false);
        opciones.SetActive(true);
        creditos.SetActive(false);
        proyectSelect.SetActive(false);
    }
    /// <summary>
    /// M�todo encargado de mostrar el Menu de creditos.
    /// </summary>
    public void CreditosActive()
    {
        titleScreen.SetActive(false);
        menuPrincipal.SetActive(false);
        opciones.SetActive(false);
        creditos.SetActive(true);
        proyectSelect.SetActive(false);
    }
    /// <summary>
    /// M�todo encargado de mostrar el Menu de seleccion de proyectos.
    /// </summary>
    public void ProyectSelectActive()
    {
        titleScreen.SetActive(false);
        menuPrincipal.SetActive(false);
        opciones.SetActive(false);
        creditos.SetActive(false);
        proyectSelect.SetActive(true);
    }
    /// <summary>
    /// M�todo encargado de cargar los diferentes proyectos.
    /// </summary>
    public void CargarProyecto1()
    {
        SceneManager.LoadScene(1);
    }
    public void CargarProyecto2()
    {
        SceneManager.LoadScene(2);
    }
    /// <summary>
    /// M�todo encargado de salir de la aplicacion.
    /// </summary>
    public void Salir()
    {
        Application.Quit();
    }
    #endregion

    /// <summary>
    /// M�todo encargado de saltar la pantalla de titulo tras apretar la tecla correspondiente.
    /// </summary>
    private void saltarTitleScreen()
    {
        if (titleScreen.activeInHierarchy==true && Input.GetKeyDown(KeyCode.Space))
        {
            MenuPrincipalActive();
        }
    }
}
